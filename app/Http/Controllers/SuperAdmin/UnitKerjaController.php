<?php

namespace App\Http\Controllers\SuperAdmin;

use App\Models\Tiket;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\UnitKerja;
use Yajra\DataTables\Facades\DataTables;

class UnitKerjaController extends Controller
{
    public function index()
    {
        $data = UnitKerja::leftJoin('tiket', 'unit_kerja.id', '=', 'tiket.unit_kerja_id')
            ->select(
                'unit_kerja.id',
                'unit_kerja.unit_kerja_karyawan',
                DB::raw('CASE WHEN COUNT(tiket.id) = 0 THEN "---" ELSE COUNT(tiket.id) END as jumlah')
            )
            ->groupBy('unit_kerja.id', 'unit_kerja.unit_kerja_karyawan')
            ->get();

        return Datatables::of($data)->make(true);
    }

    public function storeUnit(Request $request)
    {
        // Validasi input
        $request->validate([
            'unit_kerja_karyawan' => 'required',
        ], [
            'unit_kerja_karyawan.required' => 'Unit kerja karyawan wajib diisi.',
        ]);

        // Buat pengguna baru
        $unitkerja = new UnitKerja();
        $unitkerja->unit_kerja_karyawan = $request->unit_kerja_karyawan;
        $unitkerja->save();    

        // Redirect ke halaman yang sesuai setelah create sukses
        return back()->with('success', 'Unit kerja berhasil ditambahkan');
        } 
    
    public function update(Request $request)
    {
        try {

            // Validasi input jika diperlukan
            $request->validate([
                'unit_kerja_karyawan' => 'required',
            ], [
                'unit_kerja_karyawan.required' => 'Unit kerja karyawan wajib diisi.',
            ]);

            // Cari unit kerja berdasarkan ID
            $unitkerja = UnitKerja::findOrFail($request->id);

            // Update data unit kerja berdasarkan data yang dikirim dari form
            $unitkerja->unit_kerja_karyawan = $request->unit_kerja_karyawan;
            $unitkerja->save();

            // Redirect ke halaman yang sesuai setelah update sukses
            return response()->json(['success' => 'Unit Kerja berhasil diupdate']);

        } catch (\Throwable $th) {
            return response()->json(['message' => $th->getMessage() ], 500);
        }
    }
    
        
    
    function destroy(Request $request)
    {
        try {
            // Ambil semua instance UnitKerja berdasarkan ID dari request
            $unitKerjas = UnitKerja::whereIn('id', $request->dataHapus)->get();

            // Hapus setiap instance secara individu
            foreach ($unitKerjas as $unitKerja) {
                $unitKerja->delete();
            }

            // Redirect ke halaman yang sesuai setelah delete sukses
            return response()->json(['success' => 'Unit Kerja berhasil dihapus']);
        } catch (\Throwable $th) {
            return response()->json(['message' => $th->getMessage() ], 500);
        }
    }
    
}
