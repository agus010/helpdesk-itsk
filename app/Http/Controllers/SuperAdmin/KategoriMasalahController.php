<?php

namespace App\Http\Controllers\SuperAdmin;

use App\Models\Tiket;
use App\Models\UnitKerja;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class KategoriMasalahController extends Controller
{
    public function index()
    {
        // Mengambil data kolom kategori_laporan dan unit_kerja_id
        $kategories = Tiket::select('kategori_laporan', 'unit_kerja_id')->distinct()->get();

        $jumlahKategori = [];
        foreach ($kategories as $kategori) {
            $unitKerja = UnitKerja::find($kategori->unit_kerja_id);
            if ($unitKerja) {
                $unitKerjaKaryawan = $unitKerja->unit_kerja_karyawan;
            } else {
                $unitKerjaKaryawan = '---';
            }
            
            $jumlahKategori[$kategori->kategori_laporan][$unitKerjaKaryawan] = Tiket::where('kategori_laporan', $kategori->kategori_laporan)
                ->where('unit_kerja_id', $kategori->unit_kerja_id)
                ->count();
        }

        return view('superadmin.kategori_masalah', [
            'jumlahKategori' => $jumlahKategori,
        ]);
    }
}
