<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tiket extends Model
{
    use HasFactory;

    protected $table = 'tiket';
    protected $fillable = [
        'id_pengguna',
        'no_tiket',
        'nama',
        'email',
        'no_telepon',
        'nim_mahasiswa',
        'prodi',
        'nip_dosen',
        'nik_karyawan',
        'unit_kerja_id',
        'judul_tiket',
        'tingkat_urgensi',
        'kategori_laporan',
        'kategori_lainnya',
        'deskripsi_tiket',
        'waktu_tiket',
        'tanggal_masuk',
        'tanggal_pengerjaan',
        'estimasi_selesai',
        'status_tiket',
    ];

    public $timestamps = false;
    public $posisi;
    public $note;

    public function unit_kerja()
    {
        return $this->belongsTo(UnitKerja::class);
    }

    public function pengguna()
    {
        return $this->belongsTo(Pengguna::class, 'id_pengguna', 'id');
    }

    public function riwayat()
    {
        return $this->hasMany(RiwayatTiket::class, 'id_tiket', 'id');
    }

    public function lampiran()
    {
        return $this->hasMany(LampiranTiket::class, 'id_tiket', 'id');
    }
}
