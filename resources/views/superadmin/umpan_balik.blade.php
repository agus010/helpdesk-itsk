@extends('superadmin.template.main')

@section('title', 'Data Umpan Balik - Helpdesk ITSK')

@section('content')
    <div class="page-content mt-n4">
        <div class="row">
            <div class="col-md-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex justify-content-between align-items-center mb-4" id="top-content">
                            <h6 class="card-title m-0">Data Umpan Balik</h6>
                            <button type="button" onclick="hapusData()" class="btn btn-danger btn-sm btn-icon-text"
                                id="bt-del"><i class="link-icon" data-feather="x-square"></i> Hapus Data</button>
                        </div>
                        <div class="table-responsive">
                            <table id="TabelUmpanBalik" class="table hover stripe" style="width:100%">
                                <thead>
                                    <tr>
                                        <th><input type="checkbox" class="form-check-input check-all"></th>
                                        <th>Nama</th>
                                        <th>Status</th>
                                        <th>Email</th>
                                        <th>No Telp</th>
                                        <th>Pesan</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                {{-- <tbody>
                                    <tr>
                                        <td><input type="checkbox" class="form-check-input check"></td>
                                        <td>Selly</td>
                                        <td>Mahasiswa</td>
                                        <td>selly@gmail.com</td>
                                        <td><a href="https://wa.me//62852222222" target="_blank">852222222</a></td>
                                        <td>Abcdefghijklmnopqrstuvwxyz</td>
                                        <td><button type="button" id="bt-detail"
                                                class="btn btn-secondary btn-sm btn-icon-text"><i class="link-icon"
                                                    data-feather="eye" data-bs-toggle="modal"
                                                    data-bs-target="#modalDetail"></i> </button>
                                        </td>
                                    </tr>
                                </tbody> --}}
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('modals')
    <div id="modalDetail" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modalDetailLabel"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalDetailLabel">Detail Umpan Balik</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="mb-3">
                            <label for="detailNama" class="form-label">Nama:</label>
                            <input type="text" class="form-control" id="detailNama" readonly>
                        </div>
                        <div class="mb-3">
                            <label for="detailStatus" class="form-label">Status:</label>
                            <input type="text" class="form-control" id="detailStatus" readonly>
                        </div>
                        <div class="mb-3">
                            <label for="detailEmail" class="form-label">Email:</label>
                            <input type="email" class="form-control" id="detailEmail" readonly>
                        </div>
                        <div class="mb-3">
                            <label for="detailNoTelp" class="form-label">No Telp:</label>
                            <input type="text" class="form-control" id="detailNoTelp" readonly>
                        </div>
                        <div class="mb-3">
                            <label for="detailPesan" class="form-label">Pesan:</label>
                            <textarea class="form-control" id="detailPesan" rows="3" readonly></textarea>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Tutup</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script>
        const checkAllCheckbox = document.querySelector('.check-all');
        const checkboxes = document.querySelectorAll('.check');

        checkAllCheckbox.addEventListener('change', function() {
            checkboxes.forEach(function(checkbox) {
                checkbox.checked = checkAllCheckbox.checked;
            });
        });

        var tabel;
        // read data pengguna
        $(document).ready(function() {
            tabel = $('#TabelUmpanBalik').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('dataumpanbalik') }}",
                columns: [{
                        data: 'id',
                        name: 'id',
                        render: function(data, type, row, meta) {
                            return '<input type="checkbox" name="dataHapus[]" class="form-check-input check ceklis" value="' +
                                data + '" data-id="' + data + '">';
                        },
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'nama',
                        name: 'nama'
                    },
                    {
                        data: 'status',
                        name: 'status',
                    },
                    {
                        data: 'email',
                        name: 'email',

                    },
                    {
                        data: 'no_telepon',
                        name: 'no_telepon',
                    },
                    {
                        data: 'pesan',
                        name: 'pesan',
                    },
                    {
                        data: null,
                        name: 'aksi',
                        orderable: false,
                        searchable: false,
                        render: function(data, type, row, meta) {
                            return `<button type="button" onclick="modalDetail('${row.nama}','${row.status}','${row.email}','${row.no_telepon}','${row.pesan}')"class="btn btn-secondary btn-sm btn-icon-text"><i 
                                                class="link-icon" data-feather="eye" data-bs-toggle="modal"
                                                    data-bs-target="#modalDetail"></i> </button>`;
                        }
                    }
                ],
                aLengthMenu: [
                    [10, 30, 50, -1],
                    [10, 30, 50, "All"]
                ],
                iDisplayLength: 10,
                language: {
                    search: "",
                    paginate: {
                        previous: "Sebelumnya",
                        next: "Selanjutnya"
                    },
                    info: "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
                    search: "Cari:",
                    lengthMenu: "Tampilkan _MENU_ entri",
                    zeroRecords: "Tidak ditemukan data yang sesuai",
                    infoEmpty: "Menampilkan 0 sampai 0 dari 0 entri",
                    infoFiltered: "(disaring dari _MAX_ entri keseluruhan)"
                },
                rowReorder: {
                    selector: 'td:nth-child(2)'
                },
                responsive: true,
                drawCallback: function(settings) {
                    feather.replace();

                    // function modalDetail(nama, status, email, noTelp, pesan) {
                    //     $('#detailNama').val(nama);
                    //     $('#detailStatus').val(status);
                    //     $('#detailEmail').val(email);
                    //     $('#detailNoTelp').val(noTelp);
                    //     $('#detailPesan').val(pesan);
                    //     $('#modalDetail').modal('show');
                    // }

                    // $(document).on('click', '.detail-btn', function() {
                    //     var nama = $(this).data('nama');
                    //     var status = $(this).data('status');
                    //     var email = $(this).data('email');
                    //     var noTelp = $(this).data('noTelp');
                    //     var pesan = $(this).data('pesan');

                    //     modalDetail(nama, status, email, noTelp, pesan);
                    // });
                },
                initComplete: function() {
                    feather.replace();
                }
            });

            $('#TabelUmpanBalik').each(function() {
                var datatable = $(this);
                var search_input = datatable.closest('.dataTables_wrapper').find('div[id$=_filter] input');
                search_input.attr('placeholder', 'Cari');
                search_input.removeClass('form-control-sm');
                var length_sel = datatable.closest('.dataTables_wrapper').find('div[id$=_length] select');
                length_sel.removeClass('form-control-sm');
            });

            $('.check-all').on('change', function() {
                var isChecked = $(this).is(':checked');
                $('.check').prop('checked', isChecked);
            });

            tabel.on('responsive-display.dt', function(e, datatable, row, showHide, update) {
                feather.replace();
            });
        });

        // hapus data pengguna
        function hapusData() {
            var dataHapus = [];
            $('.ceklis:checked').each(function() {
                dataHapus.push($(this).val());
            });

            if (dataHapus.length > 0) {
                Swal.fire({
                    title: 'Anda yakin?',
                    text: 'Data yang dihapus tidak dapat dikembalikan!',
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#d33',
                    cancelButtonColor: '#6c757d',
                    confirmButtonText: 'Ya, hapus!',
                    cancelButtonText: 'Batal'
                }).then((result) => {
                    if (result.isConfirmed) {
                        $.ajax({
                            url: "{{ route('umpanbalik.delete') }}",
                            type: "DELETE",
                            data: {
                                dataHapus: dataHapus,
                                _token: "{{ csrf_token() }}",
                                _method: "DELETE"
                            },
                            success: function(response) {
                                console.log(response);
                                Swal.fire({
                                    title: 'Berhasil',
                                    text: 'Data berhasil dihapus',
                                    icon: 'success',
                                    confirmButtonText: 'OK'
                                });
                                tabel.ajax.reload();
                            },
                            error: function(xhr) {
                                console.log(xhr.responseJSON.message);
                                Swal.fire({
                                    title: 'Gagal',
                                    text: xhr.responseJSON.message,
                                    icon: 'error',
                                    confirmButtonText: 'OK'
                                });
                            }
                        });
                    }
                });
            } else {
                Swal.fire({
                    title: 'Peringatan',
                    text: 'Pilih data yang akan dihapus',
                    icon: 'warning',
                    confirmButtonText: 'OK'
                });
            }
        }

        function modalDetail(nama, status, email, noTelp, pesan) {
            $('#detailNama').val(nama);
            $('#detailStatus').val(status);
            $('#detailEmail').val(email);
            $('#detailNoTelp').val(noTelp);
            $('#detailPesan').val(pesan);
            $('#modalDetail').modal('show');
        }

        $(document).on('click', '.detail-btn', function() {
            var nama = $(this).data('nama');
            var status = $(this).data('status');
            var email = $(this).data('email');
            var noTelp = $(this).data('noTelp');
            var pesan = $(this).data('pesan');

            modalDetail(nama, status, email, noTelp, pesan);
        });


        $(window).resize(function() {
            $('#TabelUmpanBalik').DataTable().columns.adjust().responsive.recalc();
        });
    </script>
@endpush

@push('style')
    <style>
        .link-icon {
            max-width: 20px;
        }

        .page-item.active .page-link {
            background-color: #14A44D !important;
            border-color: #14A44D !important;
            color: white !important;
        }

        .page-link {
            color: #333333 !important;
        }

        #TabelUmpanBalik thead th:first-child {
            cursor: default;
        }

        #TabelUmpanBalik thead th:first-child::after,
        #TabelUmpanBalik thead th:first-child::before {
            display: none !important;
            pointer-events: none;
        }

        #TabelUmpanBalik td,
        #TabelUmpanBalik th {
            text-align: center;
        }

        #TabelUmpanBalik td.child {
            text-align: left;
        }

        .dataTables_empty {
            text-align: center !important;
        }

        @media only screen and (max-width: 768px) {
            #TabelUmpanBalik td {
                white-space: normal;
                word-wrap: break-word;
            }

            #TabelUmpanBalik_filter {
                margin-top: 10px;
            }
        }

        @media only screen and (max-width: 476px) {
            #top-content {
                flex-direction: column;
            }

            #bt-del {
                margin-top: 10px;
                width: 72%;
            }

        }
    </style>
@endpush
