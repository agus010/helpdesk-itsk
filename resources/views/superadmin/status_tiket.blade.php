@extends('superadmin.template.main')

@section('title', 'Data Status Tiket - Helpdesk ITSK')

@section('content')
    <div class="page-content mt-n4">
        <div class="row">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-body">
                        <h6 class="card-title">Data Status Tiket Pengaduan</h6>
                        <div class="table-responsive">
                            <table id="TabelStatusTiket" class="table hover stripe" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama Status</th>
                                        <th>Jumlah</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($jumlahStatus as $status => $jumlah)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $status }}</td>
                                            <td>{{ $jumlah }}</td>
                                    @endforeach
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 mt-4 mt-md-0">
                <div class="card">
                    <div class="card-body">
                        <h6 class="card-title">Status Tiket</h6>
                        <div id="statusTiket" data-jumlah-belumdiproses="{{ $jumlahBelumDiproses }}"
                            data-jumlah-sedangdiproses="{{ $jumlahSedangDiproses }}"
                            data-jumlah-selesai="{{ $jumlahSelesai }}"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script>
        $(function() {
            $('#TabelStatusTiket').DataTable({
                "aLengthMenu": [
                    [10, 30, 50, -1],
                    [10, 30, 50, "All"]
                ],
                "iDisplayLength": 10,
                "language": {
                    search: "",
                    "paginate": {
                        "previous": "Sebelumnya",
                        "next": "Selanjutnya"
                    },
                    "info": "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
                    "search": "Cari:",
                    "lengthMenu": "Tampilkan _MENU_ entri",
                    "zeroRecords": "Tidak ditemukan data yang sesuai",
                    "infoEmpty": "Menampilkan 0 sampai 0 dari 0 entri",
                    "infoFiltered": "(disaring dari _MAX_ entri keseluruhan)"
                },
                "responsive": true
            });

            $('#TabelStatusTiket').each(function() {
                var datatable = $(this);
                var search_input = datatable.closest('.dataTables_wrapper').find('div[id$=_filter] input');
                search_input.attr('placeholder', 'Cari');
                search_input.removeClass('form-control-sm');
                var length_sel = datatable.closest('.dataTables_wrapper').find('div[id$=_length] select');
                length_sel.removeClass('form-control-sm');
            });
        });
    </script>
@endpush

@push('style')
    <style>
        .page-item.active .page-link {
            background-color: #14A44D !important;
            border-color: #14A44D !important;
            color: white !important;
        }

        .page-link {
            color: #333333 !important;
        }

        .dataTables_empty {
            text-align: center !important;
        }

        #TabelStatusTiket td,
        #TabelStatusTiket th {
            text-align: center;
        }

        #TabelStatusTiket td.child{
            text-align: left;
        }

        @media only screen and (max-width: 768px) {
            #TabelStatusTiket_filter {
                margin-top: 10px;
            }

            #TabelStatusTiket td {
                white-space: normal;
                word-wrap: break-word;
            }
        }

    </style>
@endpush
